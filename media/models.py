# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from main.models import Product


class Media(models.Model):
    class Meta:
        verbose_name = 'Изоборажение'
        verbose_name_plural = 'Изображения'

    image = models.ImageField(verbose_name='Картинка', upload_to='product_images/')
    product = models.ForeignKey(Product, verbose_name='Продукт')

    def __unicode__(self):
        return self.product.title
