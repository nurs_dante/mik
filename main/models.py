# coding=utf-8
from ckeditor.fields import RichTextField
from django.db import models

# Create your models here.
from colors.models import Color


def image_upload_to(instance, filename):
    return "images/%s" % filename


class Advert(models.Model):
    class Meta:
        verbose_name = 'Реклама'
        verbose_name_plural = 'Реклама'

    title = models.CharField(verbose_name="Назание", max_length=120, null=True, blank=False, unique=False)
    image = models.ImageField(null=True, help_text="Size 960x640")
    active = models.BooleanField(verbose_name="Отобразить", default=False)

    def __unicode__(self):
        return self.title


class Category(models.Model):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    name = models.CharField(verbose_name="Категория", max_length=120, null=True)

    def __unicode__(self):
        return self.name


class SubCategory(models.Model):
    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'

    category = models.ManyToManyField(Category, related_name='category', verbose_name='Относится к')
    name = models.CharField(verbose_name='Название подкатегории', max_length=255)
    preview = models.ImageField(verbose_name='Картинка для подкатегории', upload_to='sebcategory_images')

    def __unicode__(self):
        return self.name


# class Company(models.Model):
#     active = models.BooleanField(verbose_name="Отобразить", default=False)
#     title = models.CharField(verbose_name="Назание", max_length=50, null=True)
#     cover = models.ImageField(verbose_name="Обложка", null=True, help_text="Size 480x480")
#     phone = models.IntegerField(verbose_name='Телефон', null=False)
#     position = models.IntegerField(verbose_name='Позиция', null=False, )
#     subcategory = models.ForeignKey(SubCategory, verbose_name="Относится к подкатегориям",
#                                     related_name='subcategory')
#
#     created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания', null=True)
#     updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата изменения', null=True)
#
#     def __unicode__(self):
#         return self.title


class Product(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    subcategory = models.ForeignKey(SubCategory, verbose_name='Относится к', null=True)
    title = models.CharField(verbose_name="Назание", max_length=50, null=True)
    preview = models.ImageField(verbose_name="Превью товара", null=True, help_text="Size &&&")
    short_description = RichTextField(null=True)
    # price = models.PositiveIntegerField(verbose_name='Цена за 1 еденицу', null=True)
    # company = models.ForeignKey(Company, related_name='company', verbose_name="Компания")
    # product_colors = models.ManyToManyField(Color, verbose_name='Цвета товара', )
    # price_rub = models.PositiveIntegerField(verbose_name='Цена в рублях', default=1, null=True)
    # price_kzt = models.PositiveIntegerField(verbose_name='Цена в тенге', default=1, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания', null=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата изменения', null=True)

    def __unicode__(self):
        return self.title
