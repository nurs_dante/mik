# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from main.models import *
from media.models import Media


class AdvertAdmin(admin.ModelAdmin):
    list_display = ['title', ]


class SubCategoryInline(admin.StackedInline):
    model = SubCategory
    extra = 1


class MediaInline(admin.StackedInline):
    model = Media


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['title', ]
    inlines = (SubCategoryInline,)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


class ProductOfCompanyAdmin(admin.ModelAdmin):
    inlines = (MediaInline,)
    list_display = ['title']


admin.site.register(Advert, AdvertAdmin)
# admin.site.register(Company, CompanyAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductOfCompanyAdmin)
admin.site.register(SubCategory)
