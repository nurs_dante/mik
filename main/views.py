# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import threading

from django.core.mail import EmailMessage
from django.http import JsonResponse

# Create your views here.
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets

from main.serializers import *
from mik.parameters import *


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class SubcategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubcategorySerializer
    filter_fields = ('category',)


# class CompanyViewSet(viewsets.ModelViewSet):
#     queryset = Company.objects.all()
#     serializer_class = CompanySerializer
#     filter_fields = ('subcategory__name',)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('-updated_at')
    serializer_class = ProductSerializer
    filter_fields = ('subcategory', 'subcategory__category',)


@csrf_exempt
def get_order(request):
    if request.POST:
        name = request.POST.get('name')
        product_id = request.POST.get('product_id')
        count_of_product = request.POST.get('count_of_product')
        phone = request.POST.get('phone')
        text = request.POST.get('text')
        t = loader.get_template('email_template/email.html')
        c = dict(product=Product.objects.get(id=product_id), phone=phone,
                 count_of_product=count_of_product, name=name, text=text)
        letter = t.render(c, request)

        thread = threading.Thread(target=send_notification_email,
                                  args=('Новое сообщение', letter, EMAIL_TO))
        thread.start()
        return JsonResponse(dict(success=True, message='Message successfully sent'))
    else:
        return JsonResponse(dict(success=False, message='Request must be POST'))


def send_notification_email(title, body, to):
    email = EmailMessage(title, body=body, to=[to])
    email.content_subtype = 'html'
    email.send()
