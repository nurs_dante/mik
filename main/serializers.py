from rest_framework import serializers

from main.models import *


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category()
        fields = '__all__'


class SubcategorySerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = SubCategory
        fields = '__all__'


# class CompanySerializer(serializers.ModelSerializer):
#     subcategory = SubcategorySerializer(many=True, read_only=True)
#
#     class Meta:
#         model = Company
#         fields = '__all__'
class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    # company = CompanySerializer()
    subcategory = SubcategorySerializer()
    product_colors = ColorSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
