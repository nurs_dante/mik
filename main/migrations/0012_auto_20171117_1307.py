# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-17 13:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_remove_product_gender'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='price_kzt',
        ),
        migrations.RemoveField(
            model_name='product',
            name='price_rub',
        ),
        migrations.RemoveField(
            model_name='product',
            name='product_colors',
        ),
    ]
