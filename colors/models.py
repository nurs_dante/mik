# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from colorful.fields import RGBColorField
from django.db import models


# Create your models here.
class Color(models.Model):
    class Meta:
        verbose_name = 'Цвет'
        verbose_name_plural = 'Цвета'

    name = models.CharField(verbose_name='Название цвета', max_length=255, null=True)
    color = RGBColorField()

    def __unicode__(self):
        return self.name
