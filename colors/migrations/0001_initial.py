# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-13 12:12
from __future__ import unicode_literals

import colorful.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0446\u0432\u0435\u0442\u0430')),
                ('color', colorful.fields.RGBColorField()),
            ],
            options={
                'verbose_name': '\u0426\u0432\u0435\u0442',
                'verbose_name_plural': '\u0426\u0432\u0435\u0442\u0430',
            },
        ),
    ]
